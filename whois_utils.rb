require 'socket'
require 'json'
require 'time'

puts '=================== LOADING WHOIS UTILS ==================='

module WhoisUtils
    def self.connect_to_whois(query_type, query)
        hostname = 'whois.rede.cert.pt'
        port = 43
        command = "-T #{query_type} #{query}"
        
        sock = TCPSocket.open(hostname, port)
        sock.puts(command)
        
        data = ''
        begin
            while true
                local_data = sock.gets
                if (local_data == nil || local_data.length == 0)
                    break
                else
                    data += local_data
                end
            end

            sock.close
        rescue SocketError
            # Do nothing
        end
                    
        begin
            return JSON.parse(data)
        rescue JSON::ParserError
            return nil
        end
    end

    def self.get_ip_info(ip)
        result = connect_to_whois('json-keys', ip)
        if result == nil
            return nil
        else
            return self.create_cache_entry(result)
        end
    end

    def self.get_cidr_subnets(cidr)
        sub_cidrs = connect_to_whois('with-id', cidr)["networks"]
        new_array = []
        for sub_cidr in sub_cidrs
          new_result = create_cache_entry(sub_cidr)
          new_array.push(new_result)
        return new_array
    end
        
    def self.get_cidr_from_result(result)
        return result["networks"][0]["cidr"]
    end
        
    def self.get_entity_from_result(result)
        return result["organizations"][0]["id"]
    end
        
    def self.get_entity_from_network_result(result)
        return result["entity_id"]
    end
        
    def self.get_cidr_from_network_result(result)
        return result["cidr"]
    end
        
    def self.create_cache_entry(result)
        if (result.key?("organizations") && result["organizations"].length > 0)
            return {
                "cidr" => get_cidr_from_result(result), 
                "entity id" => get_entity_from_result(result),
                "date" => Time.now
            }
        else
            return {
                "cidr" => get_cidr_from_network_result(result), 
                "entity id" => get_entity_from_network_result(result),
                "date" => Time.now
            }
        end
    end
    
    def self.compare_information(element1, element2)
        return element1["entity id"].count(':') <=> element2["entity id"].count(':')
    end
end
