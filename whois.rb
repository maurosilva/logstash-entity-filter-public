# encoding: utf-8
# Whois Filter
#
# This filter will fetch information about IP addresses from a field of your choosing.
#
# The Whois filter performs a lookup on a Whois server specified under the
# "server" option and parsing using the module configured at the whois_parser option.
#
# The config should look like this:
#
# filter {
#   whois {
#     type => 'type'
#     server => 'whois.rede.cert.pt'
#     ip_field => 'ip'
#     information_mapping => [['entity id', 'entity id'], ['network cidr', 'cidr']]
#     whois_parser => '/opt/whois_parser.rb'
#   }
# }
#
# The whois_parser must have a module named WhoisUtils with the following methods available:
#   - compare_information(element1, element2) - this method should take two cache entries 
#                                               and should return 1, 0 or -1 if element1 is more 
#                                               specific, equal or less specific than element2
#
#   - get_cidr_subnets(cidr) - this method could just return an empty list, but in case the Contacts database
#                              used supports it, it should connect to the database and fetch every network that 
#                              is inside the cidr given as argument. (The purpose for this method is to avoid
#                              that if a big cidr gets cached, smaller and more specific cidrs don't get used
#                              because they are "hidden" by the bigger cidr).
#
#   - get_ip_info(ip) - this method should connect to the database and return a formatted cache entry based on
#                       the ip given as argument.

# Local imports
require "logstash/filters/base"
require "logstash/namespace"

# System imports
require 'thread'
require 'ipaddr'
require 'time'
require 'socket'
require 'json'

module IPUtils
  def self.ip_to_binstr(ip)
    ip_size, ip_int = self.ip_to_int(ip)
    format_str = "%0#{ip_size}b"
    return format_str % ip_int
  end

  def self.ip_to_int(ip)
    ipaddr = IPAddr.new(ip)
    
    if ipaddr.ipv4?
      return [ipaddr.to_i, 32]
    elsif ipaddr.ipv6?
      return [ipaddr.to_i, 128]
    else
      return 0
    end
  end
      
  def self.int_to_binstr(ip, ip_size)
    format_str = "%0#{ip_size}b"
    return format_str % ip
  end
end

class BinaryTreeValues
  @value = nil
  
  private
  def sort_values
    value
    values_array = @values_array
  end
    
  public
  def push(value)
    if @value == nil
      @value = value
    else
      values_array = [value, @value]
      values_array.sort { |a,b|
        WhoisUtils.compare_information(a,b)
      }
    
      @value = values_array[-1]
    end
  end
    
  def get
    @value
  end
end
      

class BinaryTreeNode
  attr_accessor :value, :left, :right
  def initialize(value=nil, left=nil, right=nil)
    @value = value
    @left = left
    @right = right
  end
  
  def get_child(str)
    if str == '0'
      return @left
    else
      return @right
    end
  end
  
  def set_child(str, value)
    if str == '0'
      @left = value
    else
      @right = value
    end
  end
end

class CIDRDB
  def initialize(cache_time) # 1 day
    @db = BinaryTreeNode.new
    @cache_result_queue = Queue.new
    @cidr_queue = Queue.new
    @cache_time = cache_time
    
    # DEBUG
    @cache_queries = 0
    @cache_misses = 0
    @rolling_average = 0
    @rolling_queries = 0
    # END DEBUG
    
    Thread.new {
      while true
        new_cidr = @cidr_queue.pop(false) # Block until new CIDR
        sub_cidrs = WhoisUtils.get_cidr_subnets(new_cidr)
        for sub_cidr in sub_cidrs
          @cache_result_queue.push(sub_cidr)
        end
        
        write_results_to_cache
      end
    }
  end
  
  private
  def write_results_to_cache()
    while !@cache_result_queue.empty?
      cache_result = @cache_result_queue.pop(true)
      put(cache_result['cidr'], cache_result)
    end
  rescue ThreadError
    return
  end
  
  def put_tree_cidr(init_ip, prefix, info)
    current_node = @db
    for index in 0..prefix
      current_char = init_ip[index]

      if current_node.get_child(current_char) == nil
        current_node.set_child(current_char, BinaryTreeNode.new)
      end
          
      current_node = current_node.get_child(current_char)
    end
      
    if (current_node.value == nil)
      current_node.value = BinaryTreeValues.new
    end
      
    current_node.value.push(info)
  end
  
  def tree_search_ip(ip, ip_size)
    ip_str = IPUtils.int_to_binstr(ip, ip_size)
    
    current_node = @db
      
    for index in 0..ip_size
      current_char = ip_str[index]

      if current_node.get_child(current_char) == nil
        if current_node.value == nil
          return nil
        else
          
          if current_node.value != nil
            return current_node.value.get
          else
            return nil
          end
        end
      else
        current_node = current_node.get_child(current_char)
      end
    end
      
    return nil
  end
  
  public
  def put(cidr, info)
    split_cidr = cidr.split('/')
    prefix = split_cidr[1].to_i
    imask = (('1' * prefix) + '0' * (32-prefix)).to_i(2)
    ip, ip_size = IPUtils.ip_to_int(split_cidr[0])
    init_ip = IPUtils.int_to_binstr(ip & imask, ip_size)
      
    put_tree_cidr(init_ip, prefix, info)
  end
  
  def match(ip)
    start_matching = Time.now #DEBUG
    #write_results_to_cache
    (int_ip, ip_size) = IPUtils.ip_to_int(ip)

    @cache_queries += 1 #DEBUG
    result = tree_search_ip(int_ip, ip_size)
    
    cache_result = result
    if (result == nil || result["date"] == nil || ((Time.now - result["date"]) > @cache_time))
      @cache_misses += 1 #DEBUG
      cache_result = WhoisUtils.get_ip_info(ip)
      
      # Put cidr into caching queue
      if cache_result != nil
        begin
          @cidr_queue.push(cache_result["cidr"])
        rescue
          # TODO: Figure out what to do
        end
      end
    end
    
    matching_time = Time.now - start_matching #DEBUG
    @rolling_average = ((@rolling_average * @rolling_queries) + matching_time) / (@rolling_queries + 1) #DEBUG
    @rolling_queries += 1 #DEBUG
    
    puts "Cache efficiency: #{(@cache_misses * 1.0)/(@cache_queries * 1.0)} in #{@cache_queries} queries. Matching average: #{@rolling_average} seconds" #DEBUG
    
    return cache_result
  end
end

class LogStash::Filters::Whois < LogStash::Filters::Base

  config_name "whois"
  milestone 1

  # The file to load the module for parsing the whois
  # Must have the following method:
  #   - get_ip_info(ip)
  #
  # Which accepts an ip string and returns an Hash with the information
  config :whois_parser, :validate => :string, :required => true

  # The Whois server to contact.
  config :server, :validate => :string, :required => true
  
  # The field in which the ip that will be used to fetch information is.
  config :ip_field, :validate => :string, :required => true

  # Accepts an array of arrays in which for each array the first position is the key
  # for the Hash returned by the parser and the second position is the name of the
  # field in which the information is supposed to go
  config :information_mapping, :validate => :array, :required => true
  
  # The amount of time to cache each entry
  config :cache_time, :validate => :number, :default => (60 * 60 * 24)
  
  @@cache = nil
  
  private
  def initialize_cache
    if @@cache == nil
      @@cache = CIDRDB.new(@cache_time)
    end
  end
  
  def register
    require @whois_parser
    initialize_cache
  end

  public
  def filter(event)
    return unless filter?(event)
    
    if event[@ip_field] != nil
      cache_result = @@cache.match(event[@ip_field])
    
      if cache_result != nil
        for map in @information_mapping
          event[map[1]] = cache_result[map[0]]
        end
      end
    end

    filter_matched(event)
  end
end # class LogStash::Filters::Whois
